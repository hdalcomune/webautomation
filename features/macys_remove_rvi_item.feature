Feature: Remove items from RVI bar

  The RVI bar should allow the user to remove items from his bar. Items can
  be removed through RVI bar's Edit mode. After the item is removed, it can 
  only be added after revisiting the page of that item. 
  If an item goes out of stock, the item should be removed from the recently 
  viewed page. There should be a ‘REMOVE’ button on top of every item preview 
  image, after you click on ‘Edit’. The removal of the items should only be 
  completed after the user clicks on ‘Done’. If the user clicks anywhere else 
  the Edit Mode should exit, and all the items removed should be inserted back 
  into place.  

  Scenario: Edit RVI saving changes
    Given I am on a PDP page
    When I click on Edit button
    Then I should see a remove button
    When I click the remove button
    And I click "Done"
    Then I should not see the item anymore
    
  Scenario: Edit RVI without saving
    Given I am on a PDP page
    When I click on Edit button
    Then I should see a remove button
    When I click the remove button
    And I click don't click "Done"
    Then I should see the item back in the list

  Scenario: Out of stock item
    Given I visit a product
      And the product goes out of stock
    When I go to a catsplash page
    Then I should not see the product in the bar
