Feature: RVI 

  Each visited item should be stored in the "Recently Viewed Item" bar and 
  displayed only after leaving the visited product page. The bar should be 
  visible only in the catsplash and PDP pages. When more than 6 items are
  added to the bar the navigation arrows should be enable. The should not 
  store more than 30 items

  Scenario: access catsplash
    Given I am on Macys home page
    When I go to a catsplash page
    Then I should see the "recently viewed items" bar
    And I should see 0 items in the recently viewed items bar
    
  Scenario: access product
    Given I am on Macys home page
    When I a access a product page
    Then I should see the "recently viewed items" bar
    And I should see 0 items in the recently viewed items bar
    When I go to a catsplash 
    Then I should see the item in the recently viewed items bar
  
  Scenario: viewing more than 6 products
    Given I viewed more than 6 products
    When I go to a catsplash page
    Then I should see the navigation arrows for the recently viewed items bar enabled

  Scenario: viewing more than 30 itens
    Given I viewed more than 30 products
    When I go to a catsplash page
    Then I should not see more than 30 items in the recently viewed bar
