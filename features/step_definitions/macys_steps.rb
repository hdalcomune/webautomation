require "rubygems"
require "watir"
require "watir-webdriver"
require "rspec"
require "yaml"

home = "www1.macys.com"
pdp_page = "http://www1.macys.com/shop/product?ID="
pdp_item = "86800"



# ------------------- macys_view_rvi.feature  ----------------------

Given(/^I am on Macys home page$/) do
  @browser = Watir::Browser.new 
  @browser.cookies.clear
  @browser.goto home
end

When(/^I go to a catsplash page$/) do
  @browser.a(:id, "flexLabelLink_1").click
end

Then(/^I should see the "(.*?)" bar$/) do |rvi|
  @browser.div(:id, "recentlyViewedContainer").wait_until_present
  @browser.div(:id, "recentlyViewedContainer").visible? == true
end

When(/^I a access a product page$/) do
  @browser.goto pdp_page + pdp_item
end

Then(/^I should see (\d+) items in the recently viewed items bar$/) do |arg1|
  @browser.div(:text, "labelContainer").eql?('recently viewed items (0)')
end

Then(/^I should see the item in the recently viewed items bar$/) do
  if @browser.ol(id: 'rviProductList').lis.length > 2
     @browser.ol(id: 'rviProductList').li.div.a.click
  end
  #@browser.div(:name => "labelContainer").a(:index => 1)
end

Given(/^I viewed more than (\d+) products$/) do |items|
# Load cookies
  cookies = YAML::load(File.open("../../lib/cookie.yaml", 'r'))
  @browser.cookies.clear
  cookies.each do |saved_cookie|
      @browser.cookies.add saved_cookie[:name],
      saved_cookie[:value],
      :expires => saved_cookie[:expires],
      :path => saved_cookie[:path]
  end
# Test element
  @browser.reload
  @browser.ol(id: 'rviProductList').lis.length > items == true
end

Then(/^I should see the navigation arrows for the recently viewed items bar enabled$/) do
  @browser.button(:name, "Next Page").visible?
end

Then(/^I should not see more than (\d+) items in the recently viewed bar$/) do |items|
# Load cookies
  cookies = YAML::load(File.open("cookie.yaml", 'r'))
  @browser.cookies.clear
  cookies.each do |saved_cookie|
  @browser.cookies.add saved_cookie[:name],
      saved_cookie[:value],
      :expires => saved_cookie[:expires],
      :path => saved_cookie[:path]
    end

# Test element
  @browser.reload
  @browser.ol(id: 'rviProductList').lis.length == items
end


# ------------ macys_remove_rvi_item.feature ----------------------

Given(/^I am on a PDP page$/) do
  pending # express the regexp above with the code you wish you had
end

When(/^I click on Edit button$/) do
  pending # express the regexp above with the code you wish you had
end

Then(/^I should see a remove button$/) do
  pending # express the regexp above with the code you wish you had
end

When(/^I click the remove button$/) do
  pending # express the regexp above with the code you wish you had
end

When(/^I click "(.*?)"$/) do |arg1|
  pending # express the regexp above with the code you wish you had
end

Then(/^I should not see the item anymore$/) do
  pending # express the regexp above with the code you wish you had
end

When(/^I click don't click "(.*?)"$/) do |arg1|
  pending # express the regexp above with the code you wish you had
end

Then(/^I should see the item back in the list$/) do
  pending # express the regexp above with the code you wish you had
end

Given(/^I visit a product$/) do
  pending # express the regexp above with the code you wish you had
end

Given(/^the product goes out of stock$/) do
  pending # express the regexp above with the code you wish you had
end

Then(/^I should not see the product in the bar$/) do
  pending # express the regexp above with the code you wish you had
end

