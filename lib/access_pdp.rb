require "watir-webdriver"
require 'yaml'

home = "www1.macys.com"
pdp_page = "http://www1.macys.com/shop/product?ID="
pdp_list = "product"
pdp_item = "86800"	
@browser = Watir::Browser.new
@browser.cookies.clear

list = File.open(pdp_list, "r")					# Abri arquivo com lista de URLs
list.each_line do |pdp_item|							# Lê cada linha do arquivo e salva em "url"
	@browser.goto pdp_page + pdp_item         
        sleep 5
end

# Save/serialize cookies
File.open("cookie.yaml", File::CREAT | File::APPEND | File::RDWR)
File.open("cookie.yaml", 'w').write YAML::dump(@browser.cookies.to_a)

@browser.close


# Load/deserialize cookies
#cookies = YAML::load(File.open("cookie.yaml", 'r'))
#@browser.cookies.clear
#cookies.each do |saved_cookie|
#  @browser.cookies.add saved_cookie[:name],
#      saved_cookie[:value],
#      :expires => saved_cookie[:expires],
#      :path => saved_cookie[:path]
#    end
